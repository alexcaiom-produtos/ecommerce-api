/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.controller;

import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.ecommerce.api.framework.model.SessaoDeUsuario;
import br.com.massuda.alexander.ecommerce.api.servico.ServicoUsuario;
import br.com.massuda.alexander.ecommerce.api.utils.GerenciamentoDeSessoesDeUsuario;
import br.com.massuda.alexander.spring.framework.infra.web.excecoes.Erro;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

/**
 * @author Alex
 *
 */
@RestController
@RequestMapping("/login")
public class LoginController extends ControllerBase {

	@Autowired
	protected GerenciamentoDeSessoesDeUsuario gerenciamentoDeSessoesDeUsuario;
	@Autowired
	private ServicoUsuario servico;
	
	
	@CrossOrigin
	@ResponseBody
	@GetMapping("/estaLogado")
	public boolean estaLogado(String usuario) {
		return gerenciamentoDeSessoesDeUsuario.getStatusLogon(usuario);
	}
	
	@CrossOrigin
	@ResponseBody
	@PostMapping("/deslogar")
	public void deslogar(String usuario) {
		boolean estaLogado = estaLogado(usuario);
		if (estaLogado) {
			gerenciamentoDeSessoesDeUsuario.removeSessaoUsuario(usuario);
		}
	}
	
	@CrossOrigin
	@ResponseBody
	@PostMapping(value="/logar")
	public String logar (String usuario, String senha) throws Erro {
		Usuario usuarioAutenticado = servico.autenticar(usuario, senha);
		SessaoDeUsuario sessaoDeUsuario = new SessaoDeUsuario();
		sessaoDeUsuario.setUsuario(new br.com.massuda.alexander.ecommerce.api.model.Usuario(usuarioAutenticado));
		sessaoDeUsuario.setHorarioLogin(GregorianCalendar.getInstance());
		gerenciamentoDeSessoesDeUsuario.addSessaoUsuario(usuario, sessaoDeUsuario);
		return usuario;
	}
	
}
