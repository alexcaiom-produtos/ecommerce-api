/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Preco extends EntidadeModelo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5274149709614185513L;
	private double valor;
	private Vigencia vigencia;
	
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Vigencia getVigencia() {
		return vigencia;
	}
	public void setVigencia(Vigencia vigencia) {
		this.vigencia = vigencia;
	}

}
