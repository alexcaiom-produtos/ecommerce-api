/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Pagamento extends EntidadeModelo {

	/**
	 * 
	 */
	private static final long serialVersionUID = -938294015054766947L;
	
	private MeioPagamento meio;
	private int numeroParcelas;
	
	public MeioPagamento getMeio() {
		return meio;
	}
	public void setMeio(MeioPagamento meio) {
		this.meio = meio;
	}
	public int getNumeroParcelas() {
		return numeroParcelas;
	}
	public void setNumeroParcelas(int numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

}
