/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import br.com.massuda.alexander.ecommerce.api.model.Usuario;
import br.com.massuda.alexander.persistencia.jdbc.anotacoes.ChaveEstrangeira;
import br.com.massuda.alexander.persistencia.jdbc.model.EntidadeModelo;

/**
 * @author Alex
 *
 */
public class Pedido extends EntidadeModelo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8896947527533864159L;
	
	private LocalDateTime criacao = LocalDateTime.now();
	@ChaveEstrangeira(estaEmOutraTabela=true)
	private Usuario cliente;
	private List<Produto> produtos;
	private Pagamento pagamento;
	private Entrega entrega;
	private BigDecimal total;
	
	public LocalDateTime getCriacao() {
		return criacao;
	}
	public void setCriacao(LocalDateTime criacao) {
		this.criacao = criacao;
	}
	public Usuario getCliente() {
		return cliente;
	}
	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}
	public List<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}
	public Pagamento getPagamento() {
		return pagamento;
	}
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	public Entrega getEntrega() {
		return entrega;
	}
	public void setEntrega(Entrega entrega) {
		this.entrega = entrega;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	

}
