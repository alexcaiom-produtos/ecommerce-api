package br.com.massuda.alexander.ecommerce.api.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;

import br.com.massuda.alexander.spring.framework.infra.excecoes.Erro;

@ControllerAdvice
public class TratadorDeExcecoes extends br.com.massuda.alexander.spring.framework.infra.web.excecoes.tratamento.TratadorDeExcecoes {
	
	private final Logger LOGGER = LogManager.getLogger(getClass());

//	@ExceptionHandler({RestClientException.class, Erro.class, Exception.class})
//	public ResponseEntity<String> tratarErroHTTP(Exception e) throws Erro {
//		LOGGER.error("Erro:", e);
//		return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
//	}
	
}
