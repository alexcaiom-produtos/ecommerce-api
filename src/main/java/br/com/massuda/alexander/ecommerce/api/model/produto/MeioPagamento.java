/**
 * 
 */
package br.com.massuda.alexander.ecommerce.api.model.produto;

/**
 * @author Alex
 *
 */
public enum MeioPagamento {
	
	DINHEIRO,
	CARTAO,
	BOLETO,
	CREDIARIO

}
